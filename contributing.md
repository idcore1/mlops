# GENERAL
This project provides preconfigured VSCode settings for following linters and formatters used in project:

Tools:
    black : for formatting
    isort: reordering imports following pep8 guidelines
    ruff : for linting
    mypy : for static typing checks


Please install corresponding VSCode extensions for smooth contribution experience (if you use VSCode).


# PACKAGE MANAGEMENT

This project utilizes Python 3.9 and Poetry v1.8.1+ as a dependency manager.

❗Note: Before installing Poetry, if you use Conda, create and activate a new Conda env (e.g. conda create -n mlops python=3.9)


1. if you don't have poetry package manager installed, one option to install it would be $curl -sSL https://install.python-poetry.org | python3 -
3. poetry install  (this command will install required dependencies)
4. poetry shell (to activate corresponding python environment with installed dependencies)

# CODE FORMATTING

Running black formatter from command line - $python -m black ./python/training/train.py

# CODE LINTING

Running lint checks - $python -m ruff ./python/training/train.py
