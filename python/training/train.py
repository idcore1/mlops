import time

import pandas as pd
import torch
import torch.nn as nn
from sklearn.metrics import accuracy_score, f1_score
from sklearn.preprocessing import StandardScaler
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class ANN(nn.Module):
    def __init__(
        self,
        in_dim: int,
        hidden_dim_1: int,
        hidden_dim_2: int,
        hidden_dim_3: int,
        n_classes: int = 10,
        dropout: float = 0.3,
    ):
        super().__init__()

        self.layer1 = nn.Sequential(
            nn.Linear(in_features=in_dim, out_features=hidden_dim_1),
            nn.ReLU(),
            nn.BatchNorm1d(hidden_dim_1),
            nn.Dropout(dropout),
        )
        self.layer2 = nn.Sequential(
            nn.Linear(in_features=hidden_dim_1, out_features=hidden_dim_2),
            nn.ReLU(),
            nn.BatchNorm1d(hidden_dim_2),
            nn.Dropout(dropout),
        )
        self.layer3 = nn.Sequential(
            nn.Linear(in_features=hidden_dim_2, out_features=hidden_dim_3),
            nn.ReLU(),
            nn.BatchNorm1d(hidden_dim_3),
            nn.Dropout(dropout),
        )
        self.output_layer = nn.Linear(
            in_features=hidden_dim_3, out_features=n_classes
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Args:
            x (torch.Tensor): (batch_size, in_dim) the input

        Output:
            (torch.Tensor): (batch_size, n_classes) the output
        """
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.output_layer(x)

        return x


class MNIST(Dataset):
    def __init__(
        self,
        data,
    ):
        self.data = data

    # def _build(self):
    # scaler = MinMaxScaler(feature_range=())
    # scaler = StandardScaler()

    def __getitem__(self, index) -> (torch.Tensor, torch.Tensor):
        return torch.tensor(self.data.iloc[index, 1:], dtype=torch.float32).to(
            device
        ), torch.tensor(self.data.iloc[index, 0]).to(device)

    def __len__(self):
        return self.data.shape[0]


train = pd.read_csv("./datasets/MNIST/mnist_train.csv.zip")
test = pd.read_csv("./datasets/MNIST/mnist_test.csv.zip")

scaler = StandardScaler()
train.iloc[:, 1:] = scaler.fit_transform(X=train.iloc[:, 1:])
test.iloc[:, 1:] = scaler.transform(X=test.iloc[:, 1:])


train_dataset = MNIST(data=train)
test_dataset = MNIST(data=test)

train_batchsize = 1024
val_batchsize = 1024

train_dataloader = DataLoader(
    dataset=train_dataset, batch_size=train_batchsize, shuffle=True
)
test_dataloader = DataLoader(
    dataset=test_dataset, batch_size=val_batchsize, shuffle=True
)

model = ANN(
    in_dim=784,
    hidden_dim_1=784 // 2,
    hidden_dim_2=784 // 4,
    hidden_dim_3=784 // 8,
).to(device)


n_epochs = 1


lr = 1e-3
optimiser = torch.optim.Adam(model.parameters(), lr=lr)

loss_fn = torch.nn.CrossEntropyLoss()


def train_epoch(model, dataloader, optimiser):
    model.train()

    for batch in tqdm(dataloader):
        x, y = batch[0], batch[1]

        output = model(x)
        output = nn.Softmax(dim=-1)(output)
        loss = loss_fn(output, y)

        optimiser.zero_grad()
        loss.backward()
        optimiser.step()

        if sanity_check:
            break


def validate(model, dataloader):
    model.eval()
    total_loss = 0
    predictions = []
    truths = []

    with torch.no_grad():
        for batch in tqdm(dataloader):
            x, y = batch[0], batch[1]

            output = model(x)
            output = nn.Softmax(dim=-1)(output)
            loss = loss_fn(output, y)
            total_loss += loss.detach().cpu().item() / len(dataloader)

            preds = torch.argmax(output, dim=-1)
            predictions.extend(preds.cpu())
            truths.extend(y.cpu())

            if sanity_check:
                break

    acc = accuracy_score(y_true=truths, y_pred=predictions)
    f1 = f1_score(y_true=truths, y_pred=predictions, average="macro")

    return total_loss, acc, f1


sanity_check = False


# test precommit hooks
def train_model(
    model,
    train_dataloader,
    test_dataloader,
    optimiser,
):
    for epoch in range(1, n_epochs + 1):
        start_time = time.time()

        print(f"========= EPOCH {epoch} STARTED =========")
        train_epoch(
            model=model, dataloader=train_dataloader, optimiser=optimiser
        )

        print(f"========= TRAIN EVALUATION STARTED =========")
        train_val_op = validate(model=model, dataloader=train_dataloader)

        print(f"========= TEST EVALUATION STARTED =========")
        test_val_op = validate(model=model, dataloader=test_dataloader)

        print(f"END OF {epoch} EPOCH")
        print(f"| Time taken: {time.time() - start_time: 7.3f} |")
        print(
            f"| Train Loss: {train_val_op[0]: 7.3f} | Train acc: {train_val_op[1]: 1.5f} | Train f1: {train_val_op[2]: 1.5f} |"
        )
        print(
            f"| Test Loss: {test_val_op[0]: 7.3f}  | Test acc: {test_val_op[1]: 1.5f}  | Test f1: {test_val_op[2]: 1.5f}  |"
        )

        if sanity_check:
            break


train_model(
    model=model,
    train_dataloader=train_dataloader,
    test_dataloader=test_dataloader,
    optimiser=optimiser,
)
