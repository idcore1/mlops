Project structure:

datasets/ - contains datasets files for experiments

python/ - contains project code

python/pipelines - data pipelines

python/tests - project pipelines unit tests

python/training - main folder for all experiments

python/tests - contains tests to be done

notebooks - for jupyter notebooks

deployment - contains files required for model deployment


To buld docker image run:
docker build -t mlops_idcore ./
